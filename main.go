package main

import (
  "net/http"
  "github.com/gin-gonic/gin"
)

func main() {
  router := gin.Default() //new gin router initialization
  router.GET("/", func(context *gin.Context) {
    context.JSON(http.StatusOK, gin.H{"data": "Hello World Version1 !"})    
  }) // first endpoint returns Hello World
  router.Run("0.0.0.0:8000") //running application, Default port is 8080
}