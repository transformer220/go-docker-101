# go-docker-101

go with docker and docker-compose

require :
1. install docker with docker-compose
2. install git

how to :
1. git clone this repo
2. using command "cd go-docker-101"
3. using command "docker-compose up -d --build"

web access:
http://localhost:8000